Yii2 Comments


```
php composer.phar require --prefer-dist yii2mod/yii2-satcomments "*"
```


Configuration
-----------------------

**Database Migrations**

Before using Comments Widget, we'll also need to prepare the database.
```php
php yii migrate --migrationPath=@vendor/yii2mod/yii2-satcomments/migrations
```
